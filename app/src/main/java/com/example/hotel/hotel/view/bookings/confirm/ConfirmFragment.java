package com.example.hotel.hotel.view.bookings.confirm;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hotel.hotel.R;
import com.example.hotel.hotel.api.CustomerPlaceHolderApi;
import com.example.hotel.hotel.models.Customer;
import com.example.hotel.hotel.models.Transaction;
import com.example.hotel.hotel.models.global.SharedPreferencesData;
import com.example.hotel.hotel.models.request.RequestListBooking;
import com.example.hotel.hotel.models.response.ResponseService;
import com.example.hotel.hotel.view.bookingdetail.BookingDetailActivity;
import com.example.hotel.hotel.view.bookings.booking.adapter.BookingCustomAdapter;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ConfirmFragment extends Fragment {

    private ViewGroup root;
    private Activity activity;
    private SwipeRefreshLayout swipeRefreshLayout;

    // Retrofit Call API
    private Retrofit retrofit;
    private CustomerPlaceHolderApi customerPlaceHolderApi;

    // Progress Loading
    private ProgressDialog progressDialog;

    // Dialog
    private String title, sub_title;
    private Dialog dialog;
    private TextView txt_header, txt_sub_header;
    private LinearLayout linear_cancel, linear_submit;

    // Java Class
    private RequestListBooking requestListBooking;
    private ResponseService responseService;
    private Gson gson;
    private Customer customer;

    // Store Data Global
    private SharedPreferencesData sharedPreferencesData;

    // Normal Values
    private String dataCustomer;
    private String TYPE_CONFIRM = "รอยืนยันการชำระเงิน";
    private Transaction transaction;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        activity = getActivity();
        root = (ViewGroup) inflater.inflate(R.layout.fragment_bookings_confirm, container, false);

        try {
            setActionDialog();

            sharedPreferencesData = new SharedPreferencesData();
            dataCustomer = sharedPreferencesData.getStringSharedPreferences(activity, getString(R.string.key_sp_customer), getString(R.string.key_value_customer));
            gson = new Gson();
            customer = new Customer();
            customer = gson.fromJson(dataCustomer, Customer.class);

            requestListBooking = new RequestListBooking();

            // RETROFIT CALL API
            retrofit = new Retrofit.Builder().baseUrl(getString(R.string.base_url_customers))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            customerPlaceHolderApi = retrofit.create(CustomerPlaceHolderApi.class);

            // Set Progress Loading
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(getString(R.string.progress_loading));

            // Pull to Reload
            swipeRefreshLayout = root.findViewById(R.id.confirm_fm_swipe_refresh_layout);

            callServiceListBooking();

            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    // do something.
                    callServiceListBooking();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
//            showDialogAlert(getString(R.string.alert_warning), getString(R.string.alert_warning_client_error));
        }

        return root;
    }

    private void callServiceListBooking () {
        try {
            progressDialog.show();
            // Set Values to java class
            requestListBooking = new RequestListBooking(customer.getUserId(), TYPE_CONFIRM);
            Call<ResponseService> call = customerPlaceHolderApi.callListBooking(requestListBooking);

            call.enqueue(new Callback<ResponseService>() {
                @Override
                public void onResponse(Call<ResponseService> call, Response<ResponseService> response) {
                    progressDialog.dismiss();

                    if (!response.isSuccessful()) {
                        return;
                    }

                    responseService = response.body();
                    responseData(responseService);
                }

                @Override
                public void onFailure(Call<ResponseService> call, Throwable t) {
//                        textViewResult.setText(t.getMessage());
                    Toast.makeText(activity, "ERROR " + t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            showDialogAlert(getString(R.string.alert_warning_en), getString(R.string.alert_warning_client_error));
        }
    }

    public void responseData(ResponseService responseService) {
        progressDialog.show();
        if (responseService.getResCode().equals(getString(R.string.code_success))) {
            setListDataBooking(responseService.getTransaction());
        } else {
            ResponseService res = new ResponseService();
            setListDataBooking(res.getTransaction());
        }
    }

    public void setListDataBooking(List<Transaction> list) {
        ListView listView = root.findViewById(R.id.confirm_fm_lv);

        if (list.size() > 0) {
            BookingCustomAdapter adapter = new BookingCustomAdapter(root.getContext(), list);
            listView.setAdapter(adapter);
            progressDialog.dismiss();
            swipeRefreshLayout.setRefreshing(false);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                    transaction = new Transaction();
                    transaction = responseService.getTransaction().get(position);
                    gotoBookingDetailPage();

                }
            });

        } else {
            String[] values = new String[] { "ไม่มีรายการแสดง"};

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,android.R.layout.simple_list_item_1,values);
            // Assign adapter to ListView
            listView.setAdapter(adapter);
            progressDialog.dismiss();
            swipeRefreshLayout.setRefreshing(false);
        }

    }

    private void gotoBookingDetailPage() {
        Intent intent = new Intent(activity, BookingDetailActivity.class);

        Gson gson = new Gson();
        String dataCustomer = gson.toJson(transaction);

        intent.putExtra("TRANSACTION", dataCustomer);
        startActivity(intent);
        // SET ANIMATE SLIDE
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void setActionDialog() {
        // Dialog
        dialog = new Dialog(activity);
        dialog.setContentView(R.layout.dialog_popup_alert);
        txt_header = dialog.findViewById(R.id.dialog_popup_alert_txt_header);
        txt_sub_header = dialog.findViewById(R.id.dialog_popup_alert_txt_sub_header);
        linear_cancel = dialog.findViewById(R.id.dialog_popup_alert_linear_cancel);
        linear_submit = dialog.findViewById(R.id.dialog_popup_alert_linear_submit);

        linear_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        linear_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    private void showDialogAlert(String title, String sub_title) {
        txt_header.setText(title);
        txt_sub_header.setText(sub_title);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        callServiceListBooking();
//        Toast.makeText(activity, "onStart", Toast.LENGTH_SHORT).show();
    }
}

