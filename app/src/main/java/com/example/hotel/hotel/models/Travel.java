package com.example.hotel.hotel.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Travel {

    @SerializedName("TraID")
    @Expose
    private Integer traID;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("Image")
    @Expose
    private String image;
    @SerializedName("Detail")
    @Expose
    private String detail;

    public Travel(Integer traID, String title, String image, String detail) {
        this.traID = traID;
        this.title = title;
        this.image = image;
        this.detail = detail;
    }

    public Travel() {
    }

    public Integer getTraID() {
        return traID;
    }

    public void setTraID(Integer traID) {
        this.traID = traID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

}
