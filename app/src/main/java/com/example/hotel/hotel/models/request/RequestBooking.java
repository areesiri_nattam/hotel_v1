package com.example.hotel.hotel.models.request;

import java.util.Date;

public class RequestBooking {

    private int RoomID = 0;
    private int CusID = 0;
    private String DocNo = "";
    private Date CheckIn;
    private Date CheckOut;
    private int Amount = 0;

    public RequestBooking(int roomID, int cusID, String docNo, Date checkIn, Date checkOut, int amount) {
        RoomID = roomID;
        CusID = cusID;
        DocNo = docNo;
        CheckIn = checkIn;
        CheckOut = checkOut;
        Amount = amount;
    }

    public RequestBooking() {
    }

    public int getRoomID() {
        return RoomID;
    }

    public void setRoomID(int roomID) {
        RoomID = roomID;
    }

    public int getCusID() {
        return CusID;
    }

    public void setCusID(int cusID) {
        CusID = cusID;
    }

    public String getDocNo() {
        return DocNo;
    }

    public void setDocNo(String docNo) {
        DocNo = docNo;
    }

    public Date getCheckIn() {
        return CheckIn;
    }

    public void setCheckIn(Date checkIn) {
        CheckIn = checkIn;
    }

    public Date getCheckOut() {
        return CheckOut;
    }

    public void setCheckOut(Date checkOut) {
        CheckOut = checkOut;
    }

    public int getAmount() {
        return Amount;
    }

    public void setAmount(int amount) {
        Amount = amount;
    }
}
