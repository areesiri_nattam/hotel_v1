package com.example.hotel.hotel.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Transaction {

    @SerializedName("BooID")
    @Expose
    private Integer booID;
    @SerializedName("CusID")
    @Expose
    private Integer cusID;
    @SerializedName("DocNo")
    @Expose
    private String docNo;
    @SerializedName("CheckIn")
    @Expose
    private String checkIn;
    @SerializedName("CheckOut")
    @Expose
    private String checkOut;
    @SerializedName("Deposit")
    @Expose
    private Integer deposit;
    @SerializedName("Total")
    @Expose
    private Integer total;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("TimeIn")
    @Expose
    private Object timeIn;
    @SerializedName("TimeOut")
    @Expose
    private Object timeOut;
    @SerializedName("DateIn")
    @Expose
    private Object dateIn;
    @SerializedName("DateOut")
    @Expose
    private Object dateOut;
    @SerializedName("BookDetailID")
    @Expose
    private Integer bookDetailID;
    @SerializedName("RoomID")
    @Expose
    private Integer roomID;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Price")
    @Expose
    private Integer price;

    public Integer getBooID() {
        return booID;
    }

    public void setBooID(Integer booID) {
        this.booID = booID;
    }

    public Integer getCusID() {
        return cusID;
    }

    public void setCusID(Integer cusID) {
        this.cusID = cusID;
    }

    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(String checkIn) {
        this.checkIn = checkIn;
    }

    public String getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(String checkOut) {
        this.checkOut = checkOut;
    }

    public Integer getDeposit() {
        return deposit;
    }

    public void setDeposit(Integer deposit) {
        this.deposit = deposit;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(Object timeIn) {
        this.timeIn = timeIn;
    }

    public Object getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(Object timeOut) {
        this.timeOut = timeOut;
    }

    public Object getDateIn() {
        return dateIn;
    }

    public void setDateIn(Object dateIn) {
        this.dateIn = dateIn;
    }

    public Object getDateOut() {
        return dateOut;
    }

    public void setDateOut(Object dateOut) {
        this.dateOut = dateOut;
    }

    public Integer getBookDetailID() {
        return bookDetailID;
    }

    public void setBookDetailID(Integer bookDetailID) {
        this.bookDetailID = bookDetailID;
    }

    public Integer getRoomID() {
        return roomID;
    }

    public void setRoomID(Integer roomID) {
        this.roomID = roomID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

}
