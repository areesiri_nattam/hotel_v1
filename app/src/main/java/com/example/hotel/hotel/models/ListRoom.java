package com.example.hotel.hotel.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListRoom {

    @SerializedName("RoomID")
    @Expose
    private Integer roomID;
    @SerializedName("TypeID")
    @Expose
    private Integer typeID;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Amount")
    @Expose
    private Integer amount;
    @SerializedName("Price")
    @Expose
    private Integer price;
    @SerializedName("Image1")
    @Expose
    private String image1;
    @SerializedName("Image2")
    @Expose
    private String image2;
    @SerializedName("Image3")
    @Expose
    private String image3;
    @SerializedName("Image4")
    @Expose
    private String image4;
    @SerializedName("Detail")
    @Expose
    private String detail;

    public Integer getRoomID() {
        return roomID;
    }

    public void setRoomID(Integer roomID) {
        this.roomID = roomID;
    }

    public Integer getTypeID() {
        return typeID;
    }

    public void setTypeID(Integer typeID) {
        this.typeID = typeID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        image3 = image3;
    }

    public String getImage4() {
        return image4;
    }

    public void setImage4(String image4) {
        image4 = image4;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
