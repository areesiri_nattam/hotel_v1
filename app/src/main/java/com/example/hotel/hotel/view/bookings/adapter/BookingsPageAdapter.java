package com.example.hotel.hotel.view.bookings.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.hotel.hotel.view.bookings.booking.BookingFragment;
import com.example.hotel.hotel.view.bookings.cancel.CancelFragment;
import com.example.hotel.hotel.view.bookings.confirm.ConfirmFragment;

public class BookingsPageAdapter extends FragmentStatePagerAdapter {

    private int numOfTabs;

    public BookingsPageAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new BookingFragment();
            case 1:
                return new ConfirmFragment();
            case 2:
                return new CancelFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}
