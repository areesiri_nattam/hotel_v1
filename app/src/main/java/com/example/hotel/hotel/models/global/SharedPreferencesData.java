package com.example.hotel.hotel.models.global;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesData {
    private String defaultValued = "";

    public void setStringSharedPreferences (Context context, String keySP, String keyValue, String value) {
        SharedPreferences sp = context.getSharedPreferences(keySP, context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(keyValue, value);
        editor.commit();
    }

    public String getStringSharedPreferences(Context context, String keySP,String keyValue) {
        SharedPreferences sp = context.getSharedPreferences(keySP, context.MODE_PRIVATE);
        return sp.getString(keyValue, defaultValued);
    }

}
