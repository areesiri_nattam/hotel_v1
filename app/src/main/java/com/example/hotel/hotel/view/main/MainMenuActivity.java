package com.example.hotel.hotel.view.main;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.hotel.hotel.R;
import com.example.hotel.hotel.models.BottomNavigationViewHelper;
import com.example.hotel.hotel.models.Customer;
import com.example.hotel.hotel.models.global.SharedPreferencesData;
import com.example.hotel.hotel.view.bookings.BookingsFragment;
import com.example.hotel.hotel.view.home.HomeFragment;
import com.example.hotel.hotel.view.settings.SettingsFragment;
import com.example.hotel.hotel.view.travel.TravelFragment;
import com.example.hotel.hotel.view.tutorial.TutorialActivity;
import com.google.gson.Gson;

public class MainMenuActivity extends AppCompatActivity {

    private Dialog dialog;

    // Dialog
    private TextView txt_header, txt_sub_header;
    private LinearLayout linear_cancel, linear_submit;
    private Boolean isBlack = false;

    // Java Class
    private Gson gson;
    private Customer customer;

    // Store Data Global
    private SharedPreferencesData sharedPreferencesData;

    // Normal Values
    private Context context = MainMenuActivity.this;
    private String dataCustomer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        try {
            BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.main_bottom_navigation);
            bottomNavigationView.setOnNavigationItemSelectedListener(navListener);

            BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);

//            Bundle bundle = getIntent().getExtras();
//            dataCustomer = bundle.getString("CUSTOMER");
            sharedPreferencesData = new SharedPreferencesData();
            dataCustomer = sharedPreferencesData.getStringSharedPreferences(context, getString(R.string.key_sp_customer), getString(R.string.key_value_customer));
            gson = new Gson();
            customer = new Customer();
            customer = gson.fromJson(dataCustomer, Customer.class);


            Bundle bundle = new Bundle();
            Customer cs = customer;
            // Clear data promotions because the fragment is can't user data promotions.
//            cs.setPromotions(null);
            Gson gson = new Gson();
            String dataCustomer = gson.toJson(cs);

            bundle.putString(getString(R.string.key_sp_customer), dataCustomer);
            // Send Value To Fragment Page

            HomeFragment homeFragment = new HomeFragment();
            homeFragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment_container,
                    homeFragment).commit();

        } catch (Exception e) {
            e.printStackTrace();
        }

        // Dialog
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_popup_alert);
        txt_header = dialog.findViewById(R.id.dialog_popup_alert_txt_header);
        txt_sub_header = dialog.findViewById(R.id.dialog_popup_alert_txt_sub_header);
        linear_cancel = dialog.findViewById(R.id.dialog_popup_alert_linear_cancel);
        linear_submit = dialog.findViewById(R.id.dialog_popup_alert_linear_submit);

        linear_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        linear_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionBackPage();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        sharedPreferencesData = new SharedPreferencesData();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment selectedFragment = null;
                    Boolean status = true;
                    Bundle bundle = new Bundle();
                    Customer cs = customer;
                    Gson gson = new Gson();
                    String dataCustomer = gson.toJson(cs);
                    bundle.putString(getString(R.string.key_sp_customer), dataCustomer);

                    switch (item.getItemId()){
                        case R.id.nav_home:
                            // selectedFragment = new HomeFragment();
                            HomeFragment homeFragment = new HomeFragment();
                            homeFragment.setArguments(bundle);
                            selectedFragment = homeFragment;
                            getSubportFragment(selectedFragment);
                            break;
                        case R.id.nav_bookings:
                            // selectedFragment = new BookingsFragment();
                            BookingsFragment bookingsFragment = new BookingsFragment();
                            bookingsFragment.setArguments(bundle);
                            selectedFragment = bookingsFragment;
                            getSubportFragment(selectedFragment);
                            break;

                        case R.id.nav_maps:
                            // selectedFragment = new TravelFragment();
                            TravelFragment travelFragment = new TravelFragment();
                            travelFragment.setArguments(bundle);
                            selectedFragment = travelFragment;
                            getSubportFragment(selectedFragment);
                            break;

                        case R.id.nav_settings:
//                            selectedFragment = new SettingsFragment();
                            status = false;
                            txt_header.setText(getString(R.string.alert_logout_header));
                            txt_sub_header.setText(getString(R.string.alert_logout_sub_header));
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            dialog.show();
                            break;
                    }

                    return status;
                }
            };

    private void getSubportFragment (Fragment selectedFragment) {
//        Bundle bundle = new Bundle();
//        Customer cs = customer;
//        // Clear data promotions because the fragment is can't user data promotions.
//        // cs.setPromotions(null);
//        Gson gson = new Gson();
//        String dataCustomer = gson.toJson(cs);
//
//        bundle.putString(getString(R.string.key_sp_customer), dataCustomer);
//        // Send Value To Fragment Page
//        selectedFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment_container,
                selectedFragment).commit();
    }

    @Override
    public void finish() {
        onAlertDialogBlackPage();
    }

    public void onAlertDialogBlackPage() {
        txt_header.setText(getString(R.string.alert_logout_header));
        txt_sub_header.setText(getString(R.string.alert_logout_sub_header));
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public void actionBackPage(){
        Intent intent = new Intent(MainMenuActivity.this, TutorialActivity.class);
        // Clear Stack Activity
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

        // Set Animation Back Page
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        // Close Dialog
        dialog.dismiss();
    }
}

