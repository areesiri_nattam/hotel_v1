package com.example.hotel.hotel.view.home.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hotel.hotel.R;
import com.example.hotel.hotel.models.ListRoom;
import com.example.hotel.hotel.models.OnClickListenerRoom;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class RoomTypeRecyclerViewAdapter extends RecyclerView.Adapter<RoomTypeRecyclerViewAdapter.ViewHolder> {

    private static final String TAG = "RecyclerViewAdapter";
    private Dialog dialog;

    private ImageView imgClose, imageView;
    private TextView txtHeader, txtDetail, txtDetails, txtStatus;
    private Button btn_bookings;

    //vars
    private List<ListRoom> listRooms;
    private Context mContext;

    private OnClickListenerRoom onClickListenerRoom ;

    private int ss = 0;

    public RoomTypeRecyclerViewAdapter(Context context, List<ListRoom> listRooms, OnClickListenerRoom onClickListenerRoom) {
        this.listRooms = listRooms;
        mContext = context;
        this.onClickListenerRoom = onClickListenerRoom;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_room_type_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called.");

        holder.name.setText(listRooms.get(position).getName());

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Toast.makeText(mContext, listRooms.get(position).getImage1(), Toast.LENGTH_LONG).show();
                ss = position;
                dialog = new Dialog(mContext);

                dialog.setContentView(R.layout.dialog_popup_room_detail);
                imgClose = (ImageView) dialog.findViewById(R.id.dialog_popup_room_detail_img_close);
                txtHeader = (TextView) dialog.findViewById(R.id.dialog_popup_room_detail_txt_header);
                txtStatus = (TextView) dialog.findViewById(R.id.dialog_popup_room_detail_txt_status);
                txtDetail = (TextView) dialog.findViewById(R.id.dialog_popup_room_detail_txt_detail);
                txtDetails = (TextView) dialog.findViewById(R.id.dialog_popup_room_detail_txt_details);
                btn_bookings = (Button) dialog.findViewById(R.id.dialog_popup_room_btn_booking);
                imageView = dialog.findViewById(R.id.dialog_popup_room_image);

                txtStatus.setText("ว่าง");
                txtStatus.setTextColor(Color.GREEN);

                try {
                    btn_bookings.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            onClickListenerRoom.onClickRoom(listRooms.get(position));
                            dialog.dismiss();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }


                txtHeader.setText(listRooms.get(position).getName());
                txtDetail.setText("ราคาห้องพักคืนละ : " +String.valueOf(listRooms.get(position).getPrice()));
                txtDetails.setText(listRooms.get(position).getDetail());

                Picasso.get().load(listRooms.get(position).getImage1()).into(imageView);

//                Picasso.get()
//                        .load(listRooms.get(position).getImage1())
//                        .resize(400, 200)
//                        .centerCrop()
//                        .into(imageView);

                imgClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

//        holder.image.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Log.d(TAG, "onClick: clicked on an image: " + mNames.get(position));
//                Toast.makeText(mContext, mNames.get(position), Toast.LENGTH_SHORT).show();
//            }
//        });
    }


    @Override
    public int getItemCount() {
        return listRooms.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        CircleImageView image;
        TextView name;
        LinearLayout linearLayout;

        public ViewHolder(View itemView) {
            super(itemView);
//            image = itemView.findViewById(R.id.image_view);
            name = itemView.findViewById(R.id.listview_room_type_tv_name);
            linearLayout = itemView.findViewById(R.id.listview_room_type_llo);
        }
    }

}
