package com.example.hotel.hotel.view.bookings;

import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabItem;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hotel.hotel.R;
import com.example.hotel.hotel.models.Customer;
import com.example.hotel.hotel.models.global.SharedPreferencesData;
import com.example.hotel.hotel.view.bookings.adapter.BookingsPageAdapter;
import com.google.gson.Gson;

public class BookingsFragment extends Fragment {

    private Activity activity;
    private ViewGroup root;

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private BookingsPageAdapter bookingsPageAdapter;
    private TabItem tabฺBookings;
    private TabItem tabฺConfirm;
//    private TabItem tabฺCancel;

    private String dataCustomer;
    private Gson gson;
    private Customer customer;

    // Store Data Global
    private SharedPreferencesData sharedPreferencesData;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        activity = getActivity();
        root = (ViewGroup) inflater.inflate(R.layout.fragment_bookings, container, false);

        try {
            toolbar = root.findViewById(R.id.bookings_toolbar);
            toolbar.setTitle("การจอง");
            toolbar.setTitleTextColor(Color.WHITE);

            dataCustomer = getArguments().getString(getString(R.string.key_sp_customer));
            gson = new Gson();
            customer = new Customer();
            customer = gson.fromJson(dataCustomer, Customer.class);

            tabLayout = root.findViewById(R.id.bookings_tablayout);
            tabฺBookings = root.findViewById(R.id.bookings_tabitem_bookings);
            tabฺConfirm = root.findViewById(R.id.bookings_tabitem_confirm);
//            tabฺCancel = root.findViewById(R.id.bookings_tabitem_cancel);
            viewPager = root.findViewById(R.id.bookings_viewPager);

            bookingsPageAdapter = new BookingsPageAdapter(this.getFragmentManager(),  tabLayout.getTabCount());
            viewPager.setAdapter(bookingsPageAdapter);
            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    viewPager.setCurrentItem(tab.getPosition());
                    if (tab.getPosition() == 1) {
                        // Future
                    } else if (tab.getPosition() == 2) {
                        // Future
                    } else {
                        // Future
                    }
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        return root;
    }

}
