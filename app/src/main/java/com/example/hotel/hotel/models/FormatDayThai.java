package com.example.hotel.hotel.models;

public class FormatDayThai {
    private int day;
    private String[] subDays = {"อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."};
    private String[] fullDays = {"วันอาทิตย์", "วันจันทร์", "วันอังคาร", "วันพุธ", "วันพฤหัสบดี", "วันศุกร์", "วันเสาร์"};


    public FormatDayThai(int day) {
        this.day = day;
    }

    public FormatDayThai() {
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public String getFormatSubDayThai () {
        return subDays[getDay()-1];
    }

    public String getFormatFullDayThai () {
        return fullDays[getDay()-1];
    }
}
