package com.example.hotel.hotel.view.travel.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hotel.hotel.R;
import com.example.hotel.hotel.models.Travel;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Vector;

public class TravelCustomAdapter extends BaseAdapter {

    private Context mContext;
    private List<Travel> travels;
    private int[] images;

    public TravelCustomAdapter(Context mContext, List<Travel> travels, int[] images) {
        this.mContext = mContext;
        this.travels = travels;
        this.images = images;
    }

    @Override
    public int getCount() {
        return travels.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        try {
            LayoutInflater mInflater =
                    (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if(view == null)
                view = mInflater.inflate(R.layout.listview_travel_row, parent, false);

            TextView txtHeader = (TextView)view.findViewById(R.id.listview_travel_text_header);
            txtHeader.setText(travels.get(position).getTitle());
            TextView txtDetail = (TextView)view.findViewById(R.id.listview_travel_text_detail);
            txtDetail.setText(travels.get(position).getDetail());
            ImageView imageView = (ImageView)view.findViewById(R.id.listview_travel_imageview);
//            imageView.setBackgroundResource(travels.get(position).getImage());
            Picasso.get()
                    .load(travels.get(position).getImage())
                    .resize(400, 250)
                    .centerCrop()
                    .into(imageView);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }
}
