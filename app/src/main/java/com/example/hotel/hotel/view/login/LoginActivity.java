package com.example.hotel.hotel.view.login;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hotel.hotel.R;
import com.example.hotel.hotel.models.Customer;
import com.example.hotel.hotel.api.CustomerPlaceHolderApi;
import com.example.hotel.hotel.models.global.SharedPreferencesData;
import com.example.hotel.hotel.models.request.RequestLogin;
import com.example.hotel.hotel.view.forgot.ForgotPasswordActivity;
import com.example.hotel.hotel.view.main.MainMenuActivity;
import com.example.hotel.hotel.view.register.RegisterActivity;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    // Connect UI Actions
    private RelativeLayout rellay1, rellay2;
    private EditText et_username, et_password;

    // Connect UI Actions Show View Hidden
    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            rellay1.setVisibility(View.VISIBLE);
            rellay2.setVisibility(View.VISIBLE);
        }
    };

    // Retrofit Call API
    private Retrofit retrofit;
    private CustomerPlaceHolderApi customerPlaceHolderApi;

    // Progress Loading
    private ProgressDialog progressDialog;

    // Dialog
    private Dialog dialog;
    private TextView txt_header, txt_sub_header;
    private LinearLayout linear_cancel, linear_submit;

    // Store Data Global
    private SharedPreferencesData sharedPreferencesData;

    // Normal values
    private Context context = LoginActivity.this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Set Action Bar
        try {
            Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
            setSupportActionBar(toolbar);

            getSupportActionBar().setTitle("เข้าสู่ระบบ");
            toolbar.setTitleTextColor(Color.WHITE);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.orangeDark)));

            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // perform whatever you want on back arrow click
                    finish();
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                }
            });

            retrofit = new Retrofit.Builder().baseUrl(getString(R.string.base_url_customers))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            // Set Progress Dialog
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setMessage(getString(R.string.progress_loading));

        } catch (Exception e){
            e.printStackTrace();
        }

        // RETROFIT CALL API

        customerPlaceHolderApi = retrofit.create(CustomerPlaceHolderApi.class);

        rellay1 = (RelativeLayout) findViewById(R.id.login_rellay_1);
        rellay2 = (RelativeLayout) findViewById(R.id.login_rellay_2);
        et_username = (EditText) findViewById(R.id.login_et_username);
        et_password = (EditText) findViewById(R.id.login_et_password);

        // Dialog
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_popup_alert);
        txt_header = dialog.findViewById(R.id.dialog_popup_alert_txt_header);
        txt_sub_header = dialog.findViewById(R.id.dialog_popup_alert_txt_sub_header);
        linear_cancel = dialog.findViewById(R.id.dialog_popup_alert_linear_cancel);
        linear_submit = dialog.findViewById(R.id.dialog_popup_alert_linear_submit);

        linear_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                et_username.setText(null);
//                et_password.setText(null);
                dialog.dismiss();
            }
        });

        linear_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                et_username.setText(null);
//                et_password.setText(null);
                dialog.dismiss();
            }
        });

        // End Dialog

        handler.postDelayed(runnable, 2000); //2000 is the timeout for the splash

        Button btn_login = (Button) findViewById(R.id.login_btn_login);
        Button btn_register = (Button) findViewById(R.id.login_btn_register);
        Button btn_forgot = (Button) findViewById(R.id.login_btn_forgot);



        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = et_username.getText().toString();
                String password = et_password.getText().toString();
                progressDialog.show();

                try {
                    if (username.equals("") || password.equals("")) {
                        txt_header.setText(getString(R.string.alert_login_warning));
                        txt_sub_header.setText(getString(R.string.alert_login_warning_input_user_or_pass));
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog.show();
                    } else {
                        RequestLogin requestLogin = new RequestLogin(username, password);
                        Call<Customer> call = customerPlaceHolderApi.getLogin(requestLogin);

                        call.enqueue(new Callback<Customer>() {
                            @Override
                            public void onResponse(Call<Customer> call, Response<Customer> response) {
                                progressDialog.dismiss();

                                if (!response.isSuccessful()) {
                                    return;
                                }

                                Customer customer = response.body();
                                responseData(customer);
                            }

                            @Override
                            public void onFailure(Call<Customer> call, Throwable t) {
//                                Toast.makeText(LoginActivity.this, "ERROR " + t.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (Exception e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                    txt_header.setText(getString(R.string.alert_warning));
                    txt_sub_header.setText(getString(R.string.alert_warning_client_error));
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.show();
                }

            }
        });

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
                setAnimationsNextPage();
            }
        });

        btn_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
                setAnimationsNextPage();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        sharedPreferencesData = new SharedPreferencesData();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void setAnimationsNextPage() {
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void responseData(Customer customer) {
        if (customer.getResCode().equals(getString(R.string.code_success))) {
            progressDialog.show();
            gotoMainMenuActivity(customer);
        } else {
//            txt_header.setText(customer.getResCode());
            txt_header.setText(getString(R.string.alert_warning));
            txt_sub_header.setText(customer.getMessage());
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        }
    }

    public void gotoMainMenuActivity(Customer customer) {
        try {
            Gson gson = new Gson();
            String dataCustomer = gson.toJson(customer);
            sharedPreferencesData.setStringSharedPreferences(context, getString(R.string.key_sp_customer), getString(R.string.key_value_customer), dataCustomer);
            Intent intent = new Intent(LoginActivity.this, MainMenuActivity.class);
//            intent.putExtra("customer", dataCustomer);
            progressDialog.dismiss();
            startActivity(intent);
            setAnimationsNextPage();
        } catch (Exception e) {
            e.printStackTrace();
            showDialogAlert(getString(R.string.alert_warning), getString(R.string.alert_warning_client_error));
        }
    }

    public void showDialogAlert(String title, String subTitle){
        txt_header.setText(title);
        txt_sub_header.setText(subTitle);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

}
