package com.example.hotel.hotel.view.home;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hotel.hotel.R;
import com.example.hotel.hotel.api.CustomerPlaceHolderApi;
import com.example.hotel.hotel.models.Customer;
import com.example.hotel.hotel.models.FormatDayThai;
import com.example.hotel.hotel.models.FormatMountThai;
import com.example.hotel.hotel.models.ListRoom;
import com.example.hotel.hotel.models.OnClickListenerRoom;
import com.example.hotel.hotel.models.formatter.FormatterUtils;
import com.example.hotel.hotel.models.request.RequestBooking;
import com.example.hotel.hotel.models.request.RequestCheckRoom;
import com.example.hotel.hotel.models.request.RequestLogin;
import com.example.hotel.hotel.models.response.ResponseService;
import com.example.hotel.hotel.view.home.adapter.PromotionAdapter;
import com.example.hotel.hotel.view.home.adapter.RoomTypeRecyclerViewAdapter;
import com.example.hotel.hotel.view.login.LoginActivity;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeFragment extends Fragment implements OnClickListenerRoom {

    //State Values
    private String checkIn;
    private String checkOut;
        // Calendar Date
    int day ;
    int mount ;
    int year ;
    private Date dateCheckIn;
    private Date dateCheckOut;

    private Activity activity;
    private ViewGroup root;

    private ListView listView;

    // Text
    private TextView textFullName, textPhoneNo;
        // Check In
    private TextView textCheckInDate;
    private TextView textCheckInDay;
    private TextView textCheckInMount;
        // Check Out
    private TextView textCheckOutDate;
    private TextView textCheckOutDay;
    private TextView textCheckOutMount;

    private LinearLayout ll_select_rooms, ll_select_count_people, ll_btn_submit;
    private TextView textTitleSelectRoom, textTitleSelectCount;

    private static final String TAG = "HomeFragment";

    // Normal Values
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();
    private List<ListRoom> lRooms;
    private String dataCustomer;
    private Gson gson;
    private Customer customer;

    // Calendar
    private Calendar calendar;
    private DatePickerDialog datePickerDialog;

    // Format
    private FormatMountThai formatMountThai;
    private FormatDayThai formatDayThai;

    // Dialog
    private Dialog dialog;
    private TextView txt_header, txt_sub_header;
    private LinearLayout linear_cancel, linear_submit;
    private Boolean isBlack = false;

    // Java class
    private RequestBooking requestBooking ;
    private ResponseService responseService;

    // Progress Loading
    private ProgressDialog progressDialog;

    // Retrofit Call API
    private Retrofit retrofit;
    private CustomerPlaceHolderApi customerPlaceHolderApi;

    private OnClickListenerRoom onClickListenerRoom ;

    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        activity = getActivity();
        root = (ViewGroup) inflater.inflate(R.layout.fragment_home, container, false);

        try {
            dataCustomer = getArguments().getString(getString(R.string.key_sp_customer));
            gson = new Gson();
            customer = new Customer();
            customer = gson.fromJson(dataCustomer, Customer.class);

            // Set Class
            responseService = new ResponseService();
            requestBooking = new RequestBooking();
            requestBooking.setCusID(customer.getUserId());

            // Set Retrofit Call API
            retrofit = new Retrofit.Builder().baseUrl(getString(R.string.base_url_customers))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            customerPlaceHolderApi = retrofit.create(CustomerPlaceHolderApi.class);

            // Set Progress Dialog
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(getString(R.string.progress_loading));

        } catch (Exception e) {
            e.printStackTrace();
        }

        ll_select_rooms = root.findViewById(R.id.fg_home_llo_select_rooms);
//        ll_select_count_people = root.findViewById(R.id.fg_home_llo_select_count_people);
        ll_btn_submit = root.findViewById(R.id.fg_home_llo_btn_confirm);

        textTitleSelectRoom = root.findViewById(R.id.fg_home_tv_select_rooms);
//        textTitleSelectCount = root.findViewById(R.id.fg_home_tv_select_count_people);

        textFullName = root.findViewById(R.id.fg_home_tv_full_name);
        textPhoneNo = root.findViewById(R.id.fg_home_tv_phone_no);

        try {
            textFullName.setText(customer.getFullname());
            textPhoneNo.setText(customer.getPhoneNo());
        } catch (Exception e){
            e.printStackTrace();
        }

        textCheckInDate = root.findViewById(R.id.home_fg_text_check_in_date);
        textCheckInDay = root.findViewById(R.id.home_fg_text_check_in_day);
        textCheckInMount = root.findViewById(R.id.home_fg_text_check_in_mount);

        textCheckOutDate = root.findViewById(R.id.home_fg_text_check_out_date);
        textCheckOutDay = root.findViewById(R.id.home_fg_text_check_out_day);
        textCheckOutMount = root.findViewById(R.id.home_fg_text_check_out_mount);

        // Dialog
        dialog = new Dialog(activity);
        dialog.setContentView(R.layout.dialog_popup_alert);
        txt_header = dialog.findViewById(R.id.dialog_popup_alert_txt_header);
        txt_sub_header = dialog.findViewById(R.id.dialog_popup_alert_txt_sub_header);
        linear_cancel = dialog.findViewById(R.id.dialog_popup_alert_linear_cancel);
        linear_submit = dialog.findViewById(R.id.dialog_popup_alert_linear_submit);

        linear_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (responseService.getResCode().equals(getString(R.string.code_success))) {
                    dialog.dismiss();
                    reloadDataBooking();
                } else {
                    dialog.dismiss();
                }
            }
        });

        linear_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (responseService.getResCode().equals(getString(R.string.code_success))) {
                    dialog.dismiss();
                    reloadDataBooking();
                } else {
                    dialog.dismiss();
                }
            }
        });

        // End Dialog

        this.setStartCarlendar();

//        this.getImages();

        listView = (ListView) root.findViewById(R.id.home_list_view);
        // set event list view inside scrollview is working.
        listView.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        this.setListView();

        // Check In Calendar Pop Up
        LinearLayout linear_start_date = root.findViewById(R.id.home_fg_linear_layout_start_date);
        linear_start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                calendar = Calendar.getInstance();
                day = calendar.get(Calendar.DAY_OF_MONTH);
                mount = calendar.get(Calendar.MONTH);
                year = calendar.get(Calendar.YEAR);

                datePickerDialog = new DatePickerDialog(activity, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int mYear, int mMount, int mDay) {

                        day = mDay;
                        mount = mMount;
                        year = mYear;

                        Date date = getDateFromDatePicker(datePicker);
                        dateCheckIn = date;
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

                        calendar.setTime(date);
                        int numDay = calendar.get(Calendar.DAY_OF_WEEK);

                        formatMountThai = new FormatMountThai(mMount);
                        formatDayThai = new FormatDayThai(numDay);

                        setTextCheckIn(String.valueOf(mDay), formatDayThai.getFormatSubDayThai(), formatMountThai.getFormatSubMountThai(), df.format(date));

                        Toast.makeText(activity, checkIn + " " + checkOut, Toast.LENGTH_LONG).show();
                    }
                }, day, mount, year);

                calendar.add(Calendar.YEAR, 0); // subtract 2 years from now
                datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
                calendar.add(Calendar.YEAR, 2); // add 4 years to min date to have 2 years after now
                datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
                datePickerDialog.show();

                datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            // Do Stuff
                            calendar = Calendar.getInstance();
                            day = calendar.get(Calendar.DAY_OF_MONTH);
                            mount = calendar.get(Calendar.MONTH);
                            year = calendar.get(Calendar.YEAR);
                        }
                    }
                });
            }
        });

        // Check Out Calendar Pop Up
        LinearLayout linear_end_date = root.findViewById(R.id.home_fg_linear_layout_end_date);
        linear_end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                calendar = Calendar.getInstance();
                Calendar c = Calendar.getInstance();
                c.setTime(dateCheckIn);
                c.add(c.DATE, 1);
                day = c.get(c.DAY_OF_MONTH);
                mount = c.get(c.MONTH);
                year = c.get(c.YEAR);

                datePickerDialog = new DatePickerDialog(activity, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int mYear, int mMount, int mDay) {

                        Date date = getDateFromDatePicker(datePicker);
                        dateCheckOut = date;
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

                        calendar.setTime(date);
                        int numDay = calendar.get(Calendar.DAY_OF_WEEK);

                        formatMountThai = new FormatMountThai(mMount);
                        formatDayThai = new FormatDayThai(numDay);

                        setTextCheckOut(String.valueOf(mDay), formatDayThai.getFormatSubDayThai(), formatMountThai.getFormatSubMountThai(),  df.format(date));
                        calendar.setTime(dateCheckIn);
//                        Toast.makeText(activity, checkIn + " " + checkOut, Toast.LENGTH_LONG).show();
                    }
                }, day, mount, year);

//                calendar.add(Calendar.YEAR, 0); // subtract 2 years from now
                datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
//                calendar.add(Calendar.YEAR, 2); // add 4 years to min date to have 2 years after now
//                datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
                datePickerDialog.show();
            }
        });


        // Select Rooms
        ll_select_rooms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
//                    final String[] rooms = new String[lRooms.size()];
//
//                    if (lRooms.size() < 1) {
//                        showAlertDialog("ห้องพักเต็ม","กรุณาเลือกวันใหม่");
//                    } else {
//                        for (int i = 0; i < lRooms.size() ; i++) {
//                            rooms[i] = lRooms.get(i).getName();
//                        }
//
//                        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
//                        builder.setTitle("เลือกห้อง");
//                        builder.setItems(rooms , new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int position) {
//                                // the user clicked on colors[which]
//                                textTitleSelectRoom.setText(lRooms.get(position).getName());
//                                requestBooking.setRoomID(lRooms.get(position).getRoomID());
//                            }
//                        });
//                        builder.show();
//                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

//        // Select Count People
//        ll_select_count_people.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                try {
//                    final String[] count = {"1", "2", "3", "4", "5", "6" , "7" , "8" , "9" , "10"};
//
//
//                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
//                    builder.setTitle("เลือกจำนวนคน");
//                    builder.setItems(count, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int position) {
//                            // the user clicked on colors[which]
//                            textTitleSelectCount.setText(count[position]);
//                            requestBooking.setAmount(position+1);
//                        }
//                    });
//                    builder.show();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });

        ll_btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    requestBooking.setCusID(customer.getUserId());
                    requestBooking.setCheckIn(dateCheckIn);
                    requestBooking.setCheckOut(dateCheckOut);

                    // Validate Request Service Values
                    if (requestBooking.getRoomID() == 0) {
                        showAlertDialog(getString(R.string.alert_warning),getString(R.string.alert_booking_input_room));
                    }
//                    else if (requestBooking.getAmount() == 0) {
//                        showAlertDialog(getString(R.string.alert_warning),getString(R.string.alert_booking_input_amount_people));
//                    }
                    else {
                        // Call Service
                        progressDialog.show();
                        Call<ResponseService> call = customerPlaceHolderApi.callBooking(requestBooking);

                        call.enqueue(new Callback<ResponseService>() {
                            @Override
                            public void onResponse(Call<ResponseService> call, Response<ResponseService> response) {
                                progressDialog.dismiss();

                                if (!response.isSuccessful()) {
                                    return;
                                }

                                responseService = new ResponseService();
                                responseService = response.body();
                                responseData(responseService);
                            }

                            @Override
                            public void onFailure(Call<ResponseService> call, Throwable t) {
//                                Toast.makeText(LoginActivity.this, "ERROR " + t.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    showAlertDialog(getString(R.string.alert_warning_en), getString(R.string.alert_warning_client_error));
                }
            }
        });

        return root;
    }

    public void responseData(ResponseService responseService) {
        if (responseService.getResCode().equals(getString(R.string.code_success))) {
            showAlertDialog(getString(R.string.alert_warning), responseService.getMessage());
        } else {
            showAlertDialog(getString(R.string.alert_warning), responseService.getMessage());
        }
    }

    public void setListView() {
        int[] images = { R.drawable.promotion_1
//                , R.drawable.promotion_2
//                , R.drawable.promotion_3
                };

        PromotionAdapter adapter = new PromotionAdapter(getContext(), images, customer.getPromotions());

        listView.setAdapter(adapter);

    }

//    private void getImages(){
//        Log.d(TAG, "initImageBitmaps: preparing bitmaps.");
//
//        mImageUrls.add("https://c1.staticflickr.com/5/4636/25316407448_de5fbf183d_o.jpg");
//        mNames.add("Standard");
//
//        mImageUrls.add("https://i.redd.it/tpsnoz5bzo501.jpg");
//        mNames.add("Superior");
//
//        mImageUrls.add("https://i.redd.it/qn7f9oqu7o501.jpg");
//        mNames.add("Deluxe");
//
//        mImageUrls.add("https://i.redd.it/j6myfqglup501.jpg");
//        mNames.add("Suite");
//
//        mImageUrls.add("https://i.redd.it/j6myfqglup501.jpg");
//        mNames.add("VIP");
//
//        initRecyclerView();
//
//    }
//
//    private void initRecyclerView(){
//        Log.d(TAG, "initRecyclerView: init recyclerview");
//
//        LinearLayoutManager layoutManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
//        RecyclerView recyclerView = root.findViewById(R.id.booking_recycler_view);
//        recyclerView.setLayoutManager(layoutManager);
//        RoomTypeRecyclerViewAdapter adapter = new RoomTypeRecyclerViewAdapter(activity, mNames, mImageUrls);
//        recyclerView.setAdapter(adapter);
//    }

    public static java.util.Date getDateFromDatePicker(DatePicker datePicker){
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year =  datePicker.getYear();

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        return calendar.getTime();
    }

    public void setTextCheckIn(String date, String day, String mount, String fullDate) {
        textCheckInDate.setText(date);
        textCheckInDay.setText(day);
        textCheckInMount.setText(mount);

        checkIn = fullDate;

        Calendar c = Calendar.getInstance();
        c.setTime(dateCheckIn);
        c.add(c.DATE, 1);
        dateCheckOut = c.getTime();
        int numDay = c.get(Calendar.DAY_OF_WEEK);
        int dDay = c.get(Calendar.DAY_OF_MONTH);
        int dMount = c.get(Calendar.MONTH);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        formatMountThai = new FormatMountThai(dMount);
        formatDayThai = new FormatDayThai(numDay);

        this.setTextCheckOut(String.valueOf(dDay), formatDayThai.getFormatSubDayThai(), formatMountThai.getFormatSubMountThai(), df.format(dateCheckOut));

    }

    public void setTextCheckOut(String date, String day, String mount, String fullDate) {
        textCheckOutDate.setText(date);
        textCheckOutDay.setText(day);
        textCheckOutMount.setText(mount);

        checkOut = fullDate;

        setSelectRoom();
    }

    public void showAlertDialog(String title, String subTitle) {
        txt_header.setText(title);
        txt_sub_header.setText(subTitle);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public void reloadDataBooking() {
        requestBooking = new RequestBooking();
        requestBooking.setCusID(customer.getUserId());

        textTitleSelectRoom.setText("เลือกห้องพัก");
//        textTitleSelectCount.setText("จำนวนคนพัก");

        setStartCarlendar();
    }

    public void setStartCarlendar() {

        calendar = Calendar.getInstance();
        day = calendar.get(Calendar.DAY_OF_MONTH);
        mount = calendar.get(Calendar.MONTH);
        year = calendar.get(Calendar.YEAR);
        int numDay = calendar.get(Calendar.DAY_OF_WEEK);

//        calendar.add(Calendar.DATE, 1);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        checkIn = format.format(calendar.getTime());
        dateCheckIn = calendar.getTime();


        setTextCheckIn(String.valueOf(day), new FormatDayThai(numDay).getFormatSubDayThai(), new FormatMountThai(mount).getFormatSubMountThai(), checkIn);

    }

    public void setSelectRoom() {
        try {
            progressDialog.show();

            RequestCheckRoom requestListBooking = new RequestCheckRoom(checkIn, checkOut);
            Call<ResponseService> call = customerPlaceHolderApi.callCheckRoom(requestListBooking);

            call.enqueue(new Callback<ResponseService>() {
                @Override
                public void onResponse(Call<ResponseService> call, Response<ResponseService> response) {
                    progressDialog.dismiss();

                    if (!response.isSuccessful()) {
                        return;
                    }

                    ResponseService res = response.body();
                    lRooms = new ArrayList<>();
                    lRooms = res.getListRooms();
                    initRecyclerView(lRooms);
                }

                @Override
                public void onFailure(Call<ResponseService> call, Throwable t) {
//                        textViewResult.setText(t.getMessage());
                    Toast.makeText(activity, "ERROR " + t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void initRecyclerView(List<ListRoom> listRooms){

        if (listRooms.size() < 1) {
            ListRoom lr = new ListRoom();
            lr.setName("ห้องพักเต็ม");
            List<ListRoom> llrm = new ArrayList<>();
            llrm.add(lr);
            listRooms = llrm;
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        final RecyclerView recyclerView = root.findViewById(R.id.booking_recycler_view);
        recyclerView.setLayoutManager(layoutManager);
        final RoomTypeRecyclerViewAdapter adapter = new RoomTypeRecyclerViewAdapter(activity, listRooms, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onClickRoom(ListRoom room) {
        textTitleSelectRoom.setText(room.getName());
        requestBooking.setRoomID(room.getRoomID());
//        Toast.makeText(activity, "Click " + room.getName(), Toast.LENGTH_SHORT).show();
    }
}
