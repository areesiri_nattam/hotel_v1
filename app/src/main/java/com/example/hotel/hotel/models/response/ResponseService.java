package com.example.hotel.hotel.models.response;

import com.example.hotel.hotel.models.ListRoom;
import com.example.hotel.hotel.models.Transaction;
import com.example.hotel.hotel.models.Travel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseService {
    @SerializedName("status")
    @Expose
    private Integer status = 0;
    @SerializedName("resCode")
    @Expose
    private String resCode = "";
    @SerializedName("message")
    @Expose
    private String message = "";

    @SerializedName("transaction")
    @Expose
    private List<Transaction> transaction = null;

    @SerializedName("listRooms")
    @Expose
    private List<ListRoom> listRooms = null;

    @SerializedName("travel")
    @Expose
    private List<Travel> travel = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getResCode() {
        return resCode;
    }

    public void setResCode(String resCode) {
        this.resCode = resCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Transaction> getTransaction() {
        return transaction;
    }

    public void setTransaction(List<Transaction> transaction) {
        this.transaction = transaction;
    }

    public List<ListRoom> getListRooms() {
        return listRooms;
    }

    public void setListRooms(List<ListRoom> listRooms) {
        this.listRooms = listRooms;
    }

    public List<Travel> getTravel() {
        return travel;
    }

    public void setTravel(List<Travel> travel) {
        this.travel = travel;
    }
}
