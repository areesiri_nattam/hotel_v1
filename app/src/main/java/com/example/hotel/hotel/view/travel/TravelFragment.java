package com.example.hotel.hotel.view.travel;


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hotel.hotel.R;
import com.example.hotel.hotel.api.CustomerPlaceHolderApi;
import com.example.hotel.hotel.models.Transaction;
import com.example.hotel.hotel.models.Travel;
import com.example.hotel.hotel.models.request.RequestListBooking;
import com.example.hotel.hotel.models.response.ResponseService;
import com.example.hotel.hotel.view.bookings.booking.adapter.BookingCustomAdapter;
import com.example.hotel.hotel.view.travel.adapter.TravelCustomAdapter;

import java.util.List;
import java.util.Vector;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class TravelFragment extends Fragment {

    private Activity activity;
    private ViewGroup root;
    private SwipeRefreshLayout swipeRefreshLayout;


    // ListView
    private ListView listView;

    // Progress Loading
    private ProgressDialog progressDialog;

    // Dialog
    private String title, sub_title;
    private Dialog dialog;
    private TextView txt_header, txt_sub_header;
    private LinearLayout linear_cancel, linear_submit;

    // Retrofit Call API
    private Retrofit retrofit;
    private CustomerPlaceHolderApi customerPlaceHolderApi;

    // Java class
    private ResponseService responseService;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        activity = getActivity();
        root = (ViewGroup) inflater.inflate(R.layout.fragment_travel, container, false);

        try {

            swipeRefreshLayout = root.findViewById(R.id.travel_fm_swipe_refresh_layout);

            // RETROFIT CALL API
            retrofit = new Retrofit.Builder().baseUrl(getString(R.string.base_url_customers))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            customerPlaceHolderApi = retrofit.create(CustomerPlaceHolderApi.class);

            // Set Progress Loading
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(getString(R.string.progress_loading));

            setActionDialog();

            callServiceListTravels();

            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    // do something.
                    callServiceListTravels();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            showDialogAlert(getString(R.string.alert_warning_en), getString(R.string.alert_warning_client_error));
        }

        // Inflate the layout for this fragment
        return root;
    }

    public void setListView(List<Travel> travels) {
        progressDialog.show();
        int[] images = { R.drawable.hinoki_land_001
                , R.drawable.pardang_001
                , R.drawable.wad_par_mai_dang_001
        };

//        Vector<Travel> travels = new Vector<>();
//        travels.add(new Travel(1,"วัดพระเจ้าพรหมมหาราช", "No", "ที่อยู่: 119 หมู่ 9 บ้านป่าไม้แดง ตำบลหนองบัว อำเภอไชยปราการ จังหวัดเชียงใหม่ 50320"));
//        travels.add(new Travel(2,"Hinoki Land", "No", "ที่อยู่: 168 หมู่ที่ 6 ตำบล ศรีดงเย็น อำเภอ ไชยปราการ เชียงใหม่ 50320"));
//        travels.add(new Travel(,3"อุทยานแห่งชาติผาแดง", "No", "ที่อยู่: อำเภอ เชียงดาว เชียงใหม่ 50170"));
        listView = (ListView) root.findViewById(R.id.travel_fm_lv);

        if (travels.size() > 0) {
            TravelCustomAdapter adapter = new TravelCustomAdapter(getContext(), travels,images);

            listView.setAdapter(adapter);
            progressDialog.dismiss();
            swipeRefreshLayout.setRefreshing(false);

        } else {
            String[] values = new String[] { "ไม่มีรายการท่องเที่ยว"};

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,android.R.layout.simple_list_item_1,values);
            // Assign adapter to ListView
            listView.setAdapter(adapter);
            progressDialog.dismiss();
            swipeRefreshLayout.setRefreshing(false);
        }

        progressDialog.dismiss();
    }

    private void setActionDialog() {
        // Dialog
        dialog = new Dialog(activity);
        dialog.setContentView(R.layout.dialog_popup_alert);
        txt_header = dialog.findViewById(R.id.dialog_popup_alert_txt_header);
        txt_sub_header = dialog.findViewById(R.id.dialog_popup_alert_txt_sub_header);
        linear_cancel = dialog.findViewById(R.id.dialog_popup_alert_linear_cancel);
        linear_submit = dialog.findViewById(R.id.dialog_popup_alert_linear_submit);

        linear_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        linear_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    private void showDialogAlert(String title, String sub_title) {
        txt_header.setText(title);
        txt_sub_header.setText(sub_title);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void callServiceListTravels () {
        try {
            progressDialog.show();
            // Set Values to java class
            Object object = new Object();
            Call<ResponseService> call = customerPlaceHolderApi.callListTravel(object);

            call.enqueue(new Callback<ResponseService>() {
                @Override
                public void onResponse(Call<ResponseService> call, Response<ResponseService> response) {
                    progressDialog.dismiss();

                    if (!response.isSuccessful()) {
                        return;
                    }

                    responseService = new ResponseService();
                    responseService = response.body();
                    responseData(responseService);
                }

                @Override
                public void onFailure(Call<ResponseService> call, Throwable t) {
//                        textViewResult.setText(t.getMessage());
                    Toast.makeText(activity, "ERROR " + t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            showDialogAlert(getString(R.string.alert_warning_en), getString(R.string.alert_warning_client_error));
        }
    }

    public void responseData(ResponseService responseService) {
        progressDialog.show();
        if (responseService.getResCode().equals(getString(R.string.code_success))) {
            setListView(responseService.getTravel());
        } else {
            ResponseService res = new ResponseService();
            setListView(res.getTravel());
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        callServiceListTravels();

    }

}
