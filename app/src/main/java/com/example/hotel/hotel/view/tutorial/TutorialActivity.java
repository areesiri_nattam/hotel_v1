package com.example.hotel.hotel.view.tutorial;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.hotel.hotel.R;
import com.example.hotel.hotel.view.login.LoginActivity;
import com.example.hotel.hotel.view.register.RegisterActivity;

public class TutorialActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        Button btn_sign_in = (Button) findViewById(R.id.tutorial_btn_sign_in);
        Button btn_register = (Button) findViewById(R.id.tutorial_btn_register);

        btn_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TutorialActivity.this, LoginActivity.class);
                startActivity(intent);

                // SET ANIMATE SLIDE
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TutorialActivity.this, RegisterActivity.class);
                startActivity(intent);

                // SET ANIMATE SLIDE
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

    }
}
