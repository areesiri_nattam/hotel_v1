package com.example.hotel.hotel.view.bookingdetail;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hotel.hotel.R;
import com.example.hotel.hotel.api.CustomerPlaceHolderApi;
import com.example.hotel.hotel.models.Customer;
import com.example.hotel.hotel.models.FormatDayThai;
import com.example.hotel.hotel.models.FormatMountThai;
import com.example.hotel.hotel.models.Transaction;
import com.example.hotel.hotel.models.request.RequestPayment;
import com.example.hotel.hotel.models.response.ResponseService;
import com.example.hotel.hotel.view.login.LoginActivity;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BookingDetailActivity extends AppCompatActivity {

    private ImageView imageView;
    private Button btn_select_image, btn_submit;
    private TextView tv_title, tv_status, tv_docNo, tv_price;
    private TextView textCheckInDate, textCheckInDay, textCheckInMount;
    private TextView textCheckOutDate, textCheckOutDay, textCheckOutMount;

    //Image request code
    private int PICK_IMAGE_REQUEST = 1;

    //storage permission code
    private static final int STORAGE_PERMISSION_CODE = 123;

    //Bitmap to get image from gallery
    private Bitmap bitmap;

    //Uri to store the image uri
    private Uri filePath;

    // Retrofit Call API
    private Retrofit retrofit;
    private CustomerPlaceHolderApi customerPlaceHolderApi;

    // Progress Loading
    private ProgressDialog progressDialog;

    // Dialog
    private boolean popup = false;
    private String title, sub_title;
    private Dialog dialog;
    private TextView txt_header, txt_sub_header;
    private LinearLayout linear_cancel, linear_submit;

    // Java Class
    private Gson gson;
    private ResponseService responseService;
    private Transaction transaction;
    private RequestPayment requestPayment;

    // Formatter
    private FormatMountThai formatMountThai;
    private FormatDayThai formatDayThai;

    private String TYPE_BOOKING_NO = "รอชำระเงิน";
    private String TYPE_BOOKING_CON = "รอยืนยันการชำระเงิน";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_detail);

        //Requesting storage permission
        requestStoragePermission();

        // Set Action Bar
        try {
            Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
            setSupportActionBar(toolbar);

            getSupportActionBar().setTitle("รายละเอียดการจอง");
            toolbar.setTitleTextColor(Color.WHITE);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.orangeDark)));

            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // perform whatever you want on back arrow click
                    finish();
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                }
            });

            Bundle bundle = getIntent().getExtras();
            String dataTransaction = bundle.getString("TRANSACTION");
            gson = new Gson();
            transaction = new Transaction();
            transaction = gson.fromJson(dataTransaction, Transaction.class);

            retrofit = new Retrofit.Builder().baseUrl(getString(R.string.base_url_customers))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            customerPlaceHolderApi = retrofit.create(CustomerPlaceHolderApi.class);

            responseService = new ResponseService();

            textCheckInDate = findViewById(R.id.booking_detail_text_check_in_date);
            textCheckInDay = findViewById(R.id.booking_detail_text_check_in_day);
            textCheckInMount = findViewById(R.id.booking_detail_text_check_in_mount);

            textCheckOutDate = findViewById(R.id.booking_detail_text_check_out_date);
            textCheckOutDay = findViewById(R.id.booking_detail_text_check_out_day);
            textCheckOutMount = findViewById(R.id.booking_detail_text_check_out_mount);

//            SimpleDateFormat df_year = new SimpleDateFormat("yyyy");
            SimpleDateFormat df_mount = new SimpleDateFormat("MM");
            SimpleDateFormat df_day = new SimpleDateFormat("dd");

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            //                Date ci_dd_date = format.parse(transaction.getCheckIn());
//                Date co_dd_date = format.parse(transaction.getCheckOut());


            Date ci_dd_date = format.parse(transaction.getCheckIn());
            Calendar c = Calendar.getInstance();
            c.setTime(ci_dd_date);
//            c.add(Calendar.DATE, 1);
            ci_dd_date = c.getTime();

            Date co_dd_date = format.parse(transaction.getCheckOut());
            Calendar d = Calendar.getInstance();
            d.setTime(co_dd_date);
            d.add(Calendar.DATE, 1);
            co_dd_date = d.getTime();

            String ck_in = format.format(ci_dd_date);
            String ck_out = format.format(co_dd_date);

            Toast.makeText(this, ck_in + " " +ck_out, Toast.LENGTH_LONG).show();


            Calendar calendar = Calendar.getInstance();
            calendar.setTime(ci_dd_date);
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            int mount = calendar.get(Calendar.MONTH);
            int year = calendar.get(Calendar.YEAR);
            int numDay = calendar.get(Calendar.DAY_OF_WEEK);

//            int o_day = d.get(Calendar.DAY_OF_MONTH);
//            int o_mount = d.get(Calendar.MONTH);
//            int o_year = d.get(Calendar.YEAR);

            setTextCheckIn(String.valueOf(day),
                    new FormatDayThai(numDay).getFormatSubDayThai(),
                    new FormatMountThai(mount).getFormatSubMountThai(),
                    ci_dd_date);

            Calendar o_calendar = Calendar.getInstance();
            o_calendar.setTime(co_dd_date);
            int o_day = o_calendar.get(Calendar.DAY_OF_MONTH);
            int o_mount = o_calendar.get(Calendar.MONTH);
            int o_year = o_calendar.get(Calendar.YEAR);
            int o_numDay = o_calendar.get(Calendar.DAY_OF_WEEK);

            setTextCheckOut(String.valueOf(o_day),
                    new FormatDayThai(o_numDay).getFormatSubDayThai(),
                    new FormatMountThai(o_mount).getFormatSubMountThai(),
                    co_dd_date);


//            Date co_dd_date = new Date(transaction.getCheckIn());

//            Calendar calendar = Calendar.getInstance();
//            calendar.setTime(ci_dd_date);
//            int ci_mount = Integer.parseInt(df_mount.format(transaction.getCheckIn()));
//
//            int ci_numDay = calendar.get(Calendar.DAY_OF_WEEK);
//            int ci_numDate = calendar.get(Calendar.DAY_OF_MONTH);

//            formatDayThai = new FormatDayThai(ci_numDay);
//            formatMountThai = new FormatMountThai(ci_mount);
//
//            textCheckInDay.setText(String.valueOf(ci_numDate));
//            textCheckInDay.setText(formatDayThai.getFormatSubDayThai());
//            textCheckInMount.setText(formatMountThai.getFormatSubMountThai());


            tv_title = (TextView) findViewById(R.id.booking_detail_tv_title);
            tv_status = (TextView) findViewById(R.id.booking_detail_tv_status);
            tv_docNo = (TextView) findViewById(R.id.booking_detail_tv_doc_no);
            tv_price = (TextView) findViewById(R.id.booking_detail_tv_total);

            btn_submit = (Button) findViewById(R.id.booking_detail_btn_upload_bill);
            btn_select_image = (Button) findViewById(R.id.booking_detail_btn_select_image);


            tv_title.setText(transaction.getName());
            tv_docNo.setText(transaction.getDocNo());
            tv_price.setText(String.valueOf(transaction.getPrice()));

            tv_status.setText(transaction.getStatus());
            if (transaction.getStatus().equals("รอชำระเงิน")) {
                tv_status.setText(transaction.getStatus());
                tv_status.setTextColor(Color.RED);
            } else {
                tv_status.setText(transaction.getStatus());
                tv_status.setTextColor(Color.GREEN);
                btn_select_image.setEnabled(false);
                btn_select_image.setVisibility(View.GONE);
                btn_submit.setEnabled(false);
                btn_submit.setVisibility(View.GONE);
            }

            requestPayment = new RequestPayment();
            requestPayment.setAmount(transaction.getPrice());
            requestPayment.setDocNo(transaction.getDocNo());
            requestPayment.setDate(new Date());

            // DIALOG
            setActionDialog();

            // Set Progress Dialog
            progressDialog = new ProgressDialog(BookingDetailActivity.this);
            progressDialog.setMessage(getString(R.string.progress_loading));

            imageView = (ImageView) findViewById(R.id.booking_detail_image_view);

            btn_select_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showFileChooser();
                }
            });

            btn_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    uploadMultipart();
                }
            });


        } catch (Exception e){
            e.printStackTrace();
        }

    }

    /*
     * This is the method responsible for image upload
     * We need the full image path and the name for the image in this method
     * */
    public void uploadMultipart() {
        //getting name for the image
        String name = "nameTest";

        //Uploading code
        try {

            if (imageView.getDrawable() == null) {
                showDialogAlert(getString(R.string.alert_warning), getString(R.string.alert_booking_detail_input_image));
            } else {

                //getting the actual path of the image
                String path = getPath(filePath);

                String uploadId = UUID.randomUUID().toString();

                //pass it like this
                File file = new File(path);
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

                // MultipartBody.Part is used to send also the actual file name
                MultipartBody.Part imagePart = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
                // add another part within the multipart request
                RequestBody fullName = RequestBody.create(MediaType.parse("multipart/form-data"), uploadId);

//                RequestPayment requestPayment = new RequestPayment();
//                requestPayment.setAmount(2000);
//                requestPayment.setDocNo("36753444400174050");
//                requestPayment.setDate(new Date());

                Gson gson = new Gson();
                String dataPayment = gson.toJson(requestPayment);

                RequestBody jsonPayment = RequestBody.create(MediaType.parse("text/plain"), dataPayment);

                Call<ResponseService> call = customerPlaceHolderApi.callUpLoadImageBill(imagePart, jsonPayment);
                progressDialog.show();
                call.enqueue(new Callback<ResponseService>() {
                    @Override
                    public void onResponse(Call<ResponseService> call, Response<ResponseService> response) {
                        progressDialog.dismiss();

                        if (!response.isSuccessful()) {
                            return;
                        }

                        responseService = response.body();
                        responseData();
                    }

                    @Override
                    public void onFailure(Call<ResponseService> call, Throwable t) {
//                                Toast.makeText(LoginActivity.this, "ERROR " + t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

//            Toast.makeText(this, uploadId, Toast.LENGTH_LONG).show();
                //Creating a multi part request
//            new MultipartUploadRequest(this, uploadId, SyncStateContract.Constants.UPLOAD_URL)
//                    .addFileToUpload(path, "image") //Adding file
//                    .addParameter("name", name) //Adding text parameter to the request
//                    .setNotificationConfig(new UploadNotificationConfig())
//                    .setMaxRetries(2)
//                    .startUpload(); //Starting the upload
            }

        } catch (Exception exc) {
            String a = exc.getMessage();
            Toast.makeText(this, exc.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    //method to show file chooser
    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    //handling the image chooser activity result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                imageView.setImageBitmap(bitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //method to get the file path from uri
    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }

    //Requesting permission
    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }

    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {
            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void responseData() {
        if (responseService.getResCode().equals(getString(R.string.code_success))) {
//            progressDialog.show();
            showDialogAlert(getString(R.string.alert_warning), responseService.getMessage());
        } else {
            showDialogAlert(getString(R.string.alert_warning), responseService.getMessage());
        }
    }

    private void setActionDialog() {
        // Dialog
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_popup_alert);
        txt_header = dialog.findViewById(R.id.dialog_popup_alert_txt_header);
        txt_sub_header = dialog.findViewById(R.id.dialog_popup_alert_txt_sub_header);
        linear_cancel = dialog.findViewById(R.id.dialog_popup_alert_linear_cancel);
        linear_submit = dialog.findViewById(R.id.dialog_popup_alert_linear_submit);

        linear_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (responseService.getResCode().equals(getString(R.string.code_success))) {
                    dialog.dismiss();
                    finish();
                } else {
                    dialog.dismiss();
                }
            }
        });

        linear_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (responseService.getResCode().equals(getString(R.string.code_success))) {
                    dialog.dismiss();
                    finish();
                } else {
                    dialog.dismiss();
                }
            }
        });
    }

    private void showDialogAlert(String title, String sub_title) {
        txt_header.setText(title);
        txt_sub_header.setText(sub_title);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void setTextCheckIn(String date, String day, String mount, Date dateCheckOut) {
        textCheckInDate.setText(date);
        textCheckInDay.setText(day);
        textCheckInMount.setText(mount);

//        Calendar c = Calendar.getInstance();
//        c.setTime(dateCheckOut);
//        c.add(c.DATE, 1);
//
//        int numDay = c.get(Calendar.DAY_OF_WEEK);
//        int dDay = c.get(Calendar.DAY_OF_MONTH);
//        int dMount = c.get(Calendar.MONTH);
//
//        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//
//        formatMountThai = new FormatMountThai(dMount);
//        formatDayThai = new FormatDayThai(numDay);

    }

    public void setTextCheckOut(String date, String day, String mount, Date fullDate) {
        textCheckOutDate.setText(date);
        textCheckOutDay.setText(day);
        textCheckOutMount.setText(mount);

    }

}
