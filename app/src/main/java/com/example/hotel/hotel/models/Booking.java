package com.example.hotel.hotel.models;

public class Booking {

    private int booID;
    private int roomID;
    private int cusID;
    private String docNo;
    private String checkIn;
    private String checkOut;
    private int amount;
    private Double price;
    private Double deposit;
    private Double total;
    private int status;

    public Booking() {
    }

    public Booking(int booID, int roomID, int cusID, String docNo, String checkIn, String checkOut, int amount, Double price, Double deposit, Double total, int status) {
        this.booID = booID;
        this.roomID = roomID;
        this.cusID = cusID;
        this.docNo = docNo;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.amount = amount;
        this.price = price;
        this.deposit = deposit;
        this.total = total;
        this.status = status;
    }

    public int getBooID() {
        return booID;
    }

    public void setBooID(int booID) {
        this.booID = booID;
    }

    public int getRoomID() {
        return roomID;
    }

    public void setRoomID(int roomID) {
        this.roomID = roomID;
    }

    public int getCusID() {
        return cusID;
    }

    public void setCusID(int cusID) {
        this.cusID = cusID;
    }

    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(String checkIn) {
        this.checkIn = checkIn;
    }

    public String getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(String checkOut) {
        this.checkOut = checkOut;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getDeposit() {
        return deposit;
    }

    public void setDeposit(Double deposit) {
        this.deposit = deposit;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
