package com.example.hotel.hotel.models.request;

import java.util.Date;

public class RequestPayment {
    private String DocNo;
    private Date Date;
    private int Amount;
    private String Time;
    private String Slip;

    public RequestPayment() {
    }

    public RequestPayment(String docNo, java.util.Date date, int amount, String time, String slip) {
        DocNo = docNo;
        Date = date;
        Amount = amount;
        Time = time;
        Slip = slip;
    }

    public String getDocNo() {
        return DocNo;
    }

    public void setDocNo(String docNo) {
        DocNo = docNo;
    }

    public java.util.Date getDate() {
        return Date;
    }

    public void setDate(java.util.Date date) {
        Date = date;
    }

    public int getAmount() {
        return Amount;
    }

    public void setAmount(int amount) {
        Amount = amount;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getSlip() {
        return Slip;
    }

    public void setSlip(String slip) {
        Slip = slip;
    }
}
