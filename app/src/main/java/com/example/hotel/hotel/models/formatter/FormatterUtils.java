package com.example.hotel.hotel.models.formatter;

public class FormatterUtils {

    public boolean validateSize(String text, int minSize, int maxSize) {
        boolean check = false;
        String value = text;
        String[] split = value.split("");

        if (split.length >= minSize && split.length <= (maxSize + 1)) {
            check = false;
        } else {
            check = true;
        }

        return check;
    }

    public boolean validatePositionValueIsStringEN(String text, int position) {
        boolean check = false;
        String[] split = text.split("");

        String a = split[position];
        int size = split.length;
        check = split[position].matches("[a-zA-Z]+\\.?");
        String b = "";

        return check;
    }

    public boolean validateStringAndNumber(String text) {

        return text.matches("[a-zA-Z_0-9]+\\.?");
    }

}
