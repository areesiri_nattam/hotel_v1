package com.example.hotel.hotel.view.home.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hotel.hotel.R;
import com.example.hotel.hotel.models.Promotion;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PromotionAdapter extends BaseAdapter {

    private Context mContext;
    private int[] images ;
    private List<Promotion> list;

    public PromotionAdapter(Context context, int[] images, List<Promotion> list) {
        this.mContext= context;
        this.images = images;
        this.list = list;
    }


    @Override
    public int getCount() {
        int size = 0;
        if (list.size() > 0) {
            size = this.list.size();
        } else {
            size = this.images.length;
        }

        return size;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater mInflater =
                (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(view == null)
            view = mInflater.inflate(R.layout.listview_promotion_row, parent, false);

        ImageView imageView = view.findViewById(R.id.listview_image_view_promotion);

        if (list.size() > 0) {
            Picasso.get().load(this.list.get(position).getImage()).into(imageView);

//            String base64String = this.list.get(position).getImage();
//            String base64Image = base64String.split(",")[1];

//            byte[] decodedString = Base64.decode(base64Image, Base64.DEFAULT);

//            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

//            imageView.setImageBitmap(decodedByte);
//            imageView.setImageBitmap(Bitmap.createScaledBitmap(decodedByte, 400, 200, false));
        } else {
            imageView.setBackgroundResource(images[position]);
        }

        return view;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    protected int sizeOf(Bitmap data) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
            return data.getRowBytes() * data.getHeight();
        } else {
            return data.getByteCount();
        }
    }


}
