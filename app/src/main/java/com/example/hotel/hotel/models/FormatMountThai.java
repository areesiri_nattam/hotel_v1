package com.example.hotel.hotel.models;

public class FormatMountThai {
    private int mount;
    private String[] subMounts = {"ม.ค", "ก.พ", "มี.ค", "เม.ย", "พ.ค", "มิ.ย", "ก.ค", "ส.ค", "ก.ย", "ต.ค"," พ.ย", "ธ.ค"};
    private String[] fullMounts = {"มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม","พฤศจิกายน", "ธันวาคม"};

    public FormatMountThai(int mount) {
        this.mount = mount;
    }

    public FormatMountThai() {
    }


    public int getMount() {
        return mount;
    }

    public void setMount(int mount) {
        this.mount = mount;
    }

    public String getFormatSubMountThai() {

        return subMounts[getMount()];
    }

    public String getFormatFullMountThai() {

        return fullMounts[getMount()];
    }
}
