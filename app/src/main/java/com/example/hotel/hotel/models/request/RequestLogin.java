package com.example.hotel.hotel.models.request;

public class RequestLogin {

    String Username;
    String Password;

    public RequestLogin() {
    }

    public RequestLogin(String username, String password) {
        Username = username;
        Password = password;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
}
