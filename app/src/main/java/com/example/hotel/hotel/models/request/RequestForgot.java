package com.example.hotel.hotel.models.request;

public class RequestForgot {

    private String PhoneNo;

    public RequestForgot() {
    }

    public RequestForgot(String phoneNo) {
        PhoneNo = phoneNo;
    }

    public String getPhoneNo() {
        return PhoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        PhoneNo = phoneNo;
    }
}
