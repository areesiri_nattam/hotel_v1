package com.example.hotel.hotel.models;

public interface OnClickListenerRoom {
    void onClickRoom(ListRoom room);
}
