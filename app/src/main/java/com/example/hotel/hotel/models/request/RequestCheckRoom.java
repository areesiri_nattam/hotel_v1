package com.example.hotel.hotel.models.request;

public class RequestCheckRoom {

    private String CheckIn = "";
    private String CheckOut = "";

    public RequestCheckRoom() {
    }

    public RequestCheckRoom(String checkIn, String checkOut) {
        CheckIn = checkIn;
        CheckOut = checkOut;
    }

    public String getCheckIn() {
        return CheckIn;
    }

    public void setCheckIn(String checkIn) {
        CheckIn = checkIn;
    }

    public String getCheckOut() {
        return CheckOut;
    }

    public void setCheckOut(String checkOut) {
        CheckOut = checkOut;
    }
}
