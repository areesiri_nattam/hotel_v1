package com.example.hotel.hotel.api;

import com.example.hotel.hotel.models.Customer;
import com.example.hotel.hotel.models.request.RequestBooking;
import com.example.hotel.hotel.models.request.RequestCheckRoom;
import com.example.hotel.hotel.models.request.RequestForgot;
import com.example.hotel.hotel.models.request.RequestListBooking;
import com.example.hotel.hotel.models.request.RequestLogin;
import com.example.hotel.hotel.models.request.RequestPayment;
import com.example.hotel.hotel.models.request.RequestRegister;
import com.example.hotel.hotel.models.response.ResponseService;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface CustomerPlaceHolderApi {
    @POST("customers/login")
    Call<Customer> getLogin(@Body RequestLogin requestLogin);
//    Call<Customer> getLogin(@Field("Username") String username, @Field("Password") String password);

    @POST("customers/register")
    Call<Customer> regster(@Body RequestRegister requestRegister);

    @POST("customers/forgot/password")
    Call<ResponseService> getForGotPassowrd(@Body RequestForgot requestForgot);

    @POST("booking/check/room")
    Call<ResponseService> callCheckRoom(@Body RequestCheckRoom requestCheckRoom);

    @POST("booking/room")
    Call<ResponseService> callBooking(@Body RequestBooking requestBooking);

    @POST("booking/list")
    Call<ResponseService> callListBooking(@Body RequestListBooking requestListBooking);

    @POST("travels/list")
    Call<ResponseService> callListTravel(@Body Object object);

    @Multipart
    @POST("payment/add_slip")
    Call<ResponseService> callUpLoadImageBill(@Part MultipartBody.Part image, @Part("data") RequestBody requestBody);
    //Call<ResponseService> callUpLoadImageBill(@PartMap Map<String, RequestBody> params);
    //upload(@PartMap Map<String, RequestBody> params);

}
