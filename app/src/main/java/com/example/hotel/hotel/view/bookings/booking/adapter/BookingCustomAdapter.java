package com.example.hotel.hotel.view.bookings.booking.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hotel.hotel.R;
import com.example.hotel.hotel.models.Transaction;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class BookingCustomAdapter extends BaseAdapter {
    private Context mContext;
    private List<Transaction> list;

    // Status
    private String TYPE_BOOKING_NO = "รอชำระเงิน";
    private String TYPE_BOOKING_CON = "รอยืนยันการชำระเงิน";
//    private int[] resId;

    public BookingCustomAdapter(Context context, List<Transaction> list) {
        this.mContext= context;
        this.list = list;
//        this.resId = resId;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater mInflater =
                (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        try {

            if (view == null)
                view = mInflater.inflate(R.layout.listview_booking_row, parent, false);

            TextView txt_title = view.findViewById(R.id.booking_row_tv_title);
            TextView txt_total = view.findViewById(R.id.booking_row_tv_total);
            TextView txt_doc_no = view.findViewById(R.id.booking_row_tv_doc_no);
            TextView txt_check_out = view.findViewById(R.id.booking_row_tv_check_out);
            TextView txt_check_in = view.findViewById(R.id.booking_row_tv_check_in);
            TextView txt_status = view.findViewById(R.id.booking_row_tv_status);

            Transaction transaction = list.get(position);

            txt_title.setText(list.get(position).getName());
            txt_total.setText(String.valueOf(list.get(position).getPrice()));
            txt_doc_no.setText(list.get(position).getDocNo());

            txt_status.setText(list.get(position).getStatus());
            if (list.get(position).getStatus().equals("รอชำระเงิน")) {
                txt_status.setTextColor(Color.RED);
                txt_status.setText("จอง");
            } else if (list.get(position).getStatus().equals("รอยืนยันการชำระเงิน")) {
                txt_status.setText("รอยืนยัน");
                txt_status.setTextColor(Color.GREEN);
            } else {
                txt_status.setText("ยืนยัน");
                txt_status.setTextColor(Color.GREEN);
            }

            String ck_in = transaction.getCheckIn();
            String ck_out = transaction.getCheckOut();

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            //                Date ci_dd_date = format.parse(transaction.getCheckIn());
//                Date co_dd_date = format.parse(transaction.getCheckOut());

            Date ci_dd_date = format.parse(transaction.getCheckIn());
            Calendar c = Calendar.getInstance();
            c.setTime(ci_dd_date);
//            c.add(Calendar.DATE, 1);
            ci_dd_date = c.getTime();

            Date co_dd_date = format.parse(transaction.getCheckOut());
            Calendar d = Calendar.getInstance();
            d.setTime(co_dd_date);
            d.add(Calendar.DATE, 1);
            co_dd_date = d.getTime();

            txt_check_in.setText(format.format(ci_dd_date));
            txt_check_out.setText(format.format(co_dd_date));


        } catch (Exception e) {
            e.printStackTrace();
        }

//        ImageView imageView = (ImageView)view.findViewById(R.id.booking_row_img);
//        imageView.setBackgroundResource(resId[position]);

        return view;
    }
}
