package com.example.hotel.hotel.view.register;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hotel.hotel.R;
import com.example.hotel.hotel.api.CustomerPlaceHolderApi;
import com.example.hotel.hotel.models.Customer;
import com.example.hotel.hotel.models.formatter.FormatterUtils;
import com.example.hotel.hotel.models.global.SharedPreferencesData;
import com.example.hotel.hotel.models.request.RequestRegister;
import com.example.hotel.hotel.view.login.LoginActivity;
import com.example.hotel.hotel.view.main.MainMenuActivity;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterActivity extends AppCompatActivity {

    // Conncect UI Actions
    private EditText et_firstname, et_lasetname, et_phonenumber, et_address, et_username, et_password, et_confrim_password;
    private Button btn_submit;

    // Retrofit Call API
    private Retrofit retrofit;
    private CustomerPlaceHolderApi customerPlaceHolderApi;

    // Progress Loading
    private ProgressDialog progressDialog;

    // Dialog
    private boolean popup = false;
    private String title, sub_title;
    private Dialog dialog;
    private TextView txt_header, txt_sub_header;
    private LinearLayout linear_cancel, linear_submit;

    // Formatter
    private FormatterUtils formatterUtils;

    // Store Data Global
    private SharedPreferencesData sharedPreferencesData;

    // Java Class
    private Customer customer;

    // Normal values
    private Context context = RegisterActivity.this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // Set Action Bar
        try {
            Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
            setSupportActionBar(toolbar);

            getSupportActionBar().setTitle(getString(R.string.txt_register));
            toolbar.setTitleTextColor(Color.WHITE);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.orangeDark)));

            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // perform whatever you want on back arrow click
                    finish();
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                }
            });

            // RETROFIT CALL API
            retrofit = new Retrofit.Builder().baseUrl(getString(R.string.base_url_customers))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            customerPlaceHolderApi = retrofit.create(CustomerPlaceHolderApi.class);

            // Set Progress Loading
            progressDialog = new ProgressDialog(RegisterActivity.this);
            progressDialog.setMessage(getString(R.string.progress_loading));

            customer = new Customer();

        } catch (Exception e){
            e.printStackTrace();
        }

        // DIALOG
        setActionDialog();


        et_firstname = (EditText) findViewById(R.id.register_txt_first_name);
        et_lasetname = (EditText) findViewById(R.id.register_txt_last_name);
        et_phonenumber = (EditText) findViewById(R.id.register_txt_phone_no);
        et_address = (EditText) findViewById(R.id.register_txt_address);
        et_username = (EditText) findViewById(R.id.register_txt_username);
        et_password = (EditText) findViewById(R.id.register_txt_password);
        et_confrim_password = (EditText) findViewById(R.id.register_txt_confirm_password);
        btn_submit = (Button) findViewById(R.id.register_btn_submit);

        // Action Click Button Submit
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean CEHCK = validateTextRegister();

                if(validateTextRegister()) {
                    showDialogAlert(title, sub_title);
                } else {
                    try {
                        // Set Values to java class
                        RequestRegister requestRegister = new RequestRegister();
                        requestRegister.setFirstName(et_firstname.getText().toString());
                        requestRegister.setLastName(et_lasetname.getText().toString());
                        requestRegister.setPhoneNo(et_phonenumber.getText().toString());
                        requestRegister.setAddress(et_address.getText().toString());
                        requestRegister.setUserName(et_username.getText().toString());
                        requestRegister.setPassWord(et_password.getText().toString());

                        Call<Customer> call = customerPlaceHolderApi.regster(requestRegister);

                        call.enqueue(new Callback<Customer>() {
                            @Override
                            public void onResponse(Call<Customer> call, Response<Customer> response) {
                                progressDialog.dismiss();

                                if (!response.isSuccessful()) {

                                    return;
                                }

                                Object object = response.body();

                                customer = new Customer();
                                customer = response.body();

                                responseData(customer);
                            }

                            @Override
                            public void onFailure(Call<Customer> call, Throwable t) {
//                        textViewResult.setText(t.getMessage());
                                Toast.makeText(RegisterActivity.this, "ERROR " + t.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                        showDialogAlert(getString(R.string.alert_warning_en), getString(R.string.alert_warning_client_error));
                    }
                }
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        sharedPreferencesData = new SharedPreferencesData();
    }

    public boolean validateTextRegister() {
        boolean check = false;
        formatterUtils = new FormatterUtils();

        String pass = et_password.getText().toString();
        String con_pass = et_confrim_password.getText().toString();

        if (et_firstname.getText().toString().equals("")) {
            title = getString(R.string.alert_warning);
            sub_title = getString(R.string.alert_register_input_first_name);
            check = true;
        } else if (et_lasetname.getText().toString().equals("")) {
            title = getString(R.string.alert_warning);
            sub_title = getString(R.string.alert_register_input_last_name);
            check = true;
        } else if (et_phonenumber.getText().toString().equals("")) {
            title = getString(R.string.alert_warning);
            sub_title = getString(R.string.alert_register_input_phone_no);
            check = true;
        } else if (et_address.getText().toString().equals("")) {
            title = getString(R.string.alert_warning);
            sub_title = getString(R.string.alert_register_input_address);
            check = true;
        } else if (et_username.getText().toString().equals("")) {
            title = getString(R.string.alert_warning);
            sub_title = getString(R.string.alert_register_input_username);
            check = true;
        } else if (formatterUtils.validateSize(et_username.getText().toString(),8,20)) {
            // Check size user name
            title = getString(R.string.alert_warning);
            sub_title = getString(R.string.alert_register_input_username_size);
            check = true;
        } else if (!formatterUtils.validatePositionValueIsStringEN(et_username.getText().toString(), 1)) {
            // Check first character user name is string only
            title = getString(R.string.alert_warning);
            sub_title = getString(R.string.alert_register_input_username_first_string);
//            et_username.setText(null);
            check = true;
        } else if (!formatterUtils.validateStringAndNumber(et_username.getText().toString())) {
            // Check user name is string or number only
            title = getString(R.string.alert_warning);
            sub_title = getString(R.string.alert_register_input_username_string);
//            et_username.setText(null);
            check = true;
        } else if (et_password.getText().toString().equals("")) {
            title = getString(R.string.alert_warning);
            sub_title = getString(R.string.alert_register_input_password);
            check = true;
        } else if (formatterUtils.validateSize(et_password.getText().toString(),8,20)) {
            // Check size password
            title = getString(R.string.alert_warning);
            sub_title = getString(R.string.alert_register_input_password_size);
            check = true;
        } else if (!formatterUtils.validateStringAndNumber(et_password.getText().toString())) {
            // Check password is string or number only
            title = getString(R.string.alert_warning);
            sub_title = getString(R.string.alert_register_input_password_string);
//            et_password.setText(null);
            check = true;
        } else if (!con_pass.equals(pass)) {
            // Check Confirm Password
            title = getString(R.string.alert_warning);
            sub_title = getString(R.string.alert_register_input_confirm_password);
            check = true;
        } else {
            check = false;
        }

        return check;
    }

    public void responseData(Customer customer) {
        try {
            if (customer.getResCode().equals(getString(R.string.code_success))) {
//                gotoMainMenuActivity();
                showDialogAlert(getString(R.string.alert_warning), customer.getMessage());
            } else {
//                txt_header.setText(customer.getResCode());
                txt_header.setText(getString(R.string.alert_warning));
                txt_sub_header.setText(customer.getMessage());
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            showDialogAlert(getString(R.string.alert_warning_en), getString(R.string.alert_warning_client_error));
        }
    }

    private void gotoMainMenuActivity() {
        try {

            Gson gson = new Gson();
            String dataCustomer = gson.toJson(customer);
            sharedPreferencesData.setStringSharedPreferences(context, getString(R.string.key_sp_customer), getString(R.string.key_value_customer), dataCustomer);

            Intent intent = new Intent(RegisterActivity.this, MainMenuActivity.class);
            startActivity(intent);
            finish();
            // SET ANIMATE SLIDE
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setActionDialog() {
        // Dialog
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_popup_alert);
        txt_header = dialog.findViewById(R.id.dialog_popup_alert_txt_header);
        txt_sub_header = dialog.findViewById(R.id.dialog_popup_alert_txt_sub_header);
        linear_cancel = dialog.findViewById(R.id.dialog_popup_alert_linear_cancel);
        linear_submit = dialog.findViewById(R.id.dialog_popup_alert_linear_submit);

        linear_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String g = customer.getResCode();
                    if (customer.getResCode().equals("")) {
                        dialog.dismiss();
                    } else {
                        if (customer.getResCode().equals(getString(R.string.code_success))) {
                            gotoMainMenuActivity();
                        } else {
                            dialog.dismiss();
                        }
                    }
                } catch (Exception e) {
                    dialog.dismiss();
                    e.printStackTrace();
                }
            }
        });

        linear_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (customer.getResCode() != "") {
                    if (customer.getResCode().equals(getString(R.string.code_success))) {
                        gotoMainMenuActivity();
                    } else {
                        dialog.dismiss();
                    }
                } else {
                    dialog.dismiss();
                }
            }
        });
    }

    private void showDialogAlert(String title, String sub_title) {
        txt_header.setText(title);
        txt_sub_header.setText(sub_title);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    // Click Back Android
    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
