package com.example.hotel.hotel.view.forgot;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.hotel.hotel.R;
import com.example.hotel.hotel.api.CustomerPlaceHolderApi;
import com.example.hotel.hotel.models.request.RequestForgot;
import com.example.hotel.hotel.models.response.ResponseService;
import com.example.hotel.hotel.view.login.LoginActivity;
import com.example.hotel.hotel.view.tutorial.TutorialActivity;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ForgotPasswordActivity extends AppCompatActivity {

    // Connect UI Actions Show View Hidden
    private EditText et_phone;
    private Button btn_submit;

    // Retrofit Call API
    private Retrofit retrofit;
    private CustomerPlaceHolderApi customerPlaceHolderApi;

    // Progress Loading
    private ProgressDialog progressDialog;

    // Dialog
    private Dialog dialog;
    private TextView txt_header, txt_sub_header;
    private LinearLayout linear_cancel, linear_submit;

    // Java Class
    private RequestForgot requestForgot;
    private ResponseService responseService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        // Set Action Bar
        try {
            Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
            setSupportActionBar(toolbar);

            getSupportActionBar().setTitle(getString(R.string.txt_forgot_password));
            toolbar.setTitleTextColor(Color.WHITE);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.orangeDark)));

            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // perform whatever you want on back arrow click
                    finish();
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                }
            });

            // RETROFIT CALL API
            retrofit = new Retrofit.Builder().baseUrl(getString(R.string.base_url_customers))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            customerPlaceHolderApi = retrofit.create(CustomerPlaceHolderApi.class);

            responseService = new ResponseService();

            // Set Progress Dialog
            progressDialog = new ProgressDialog(ForgotPasswordActivity.this);
            progressDialog.setMessage(getString(R.string.progress_loading));

            // Dialog
            dialog = new Dialog(this);
            dialog.setContentView(R.layout.dialog_popup_alert);
            txt_header = dialog.findViewById(R.id.dialog_popup_alert_txt_header);
            txt_sub_header = dialog.findViewById(R.id.dialog_popup_alert_txt_sub_header);
            linear_cancel = dialog.findViewById(R.id.dialog_popup_alert_linear_cancel);
            linear_submit = dialog.findViewById(R.id.dialog_popup_alert_linear_submit);

            linear_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (responseService.getResCode().equals(getString(R.string.code_success))) {
                        dialog.dismiss();
                        gotoTutorailActivity();
                    } else {
                        dialog.dismiss();
//                        progressDialog.show();
                    }
                }
            });

            linear_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (responseService.getResCode().equals(getString(R.string.code_success))) {
                        dialog.dismiss();
                        gotoTutorailActivity();
                    } else {
                        dialog.dismiss();
//                        progressDialog.show();
                    }
                }
            });

            // EditText and Button
            et_phone = (EditText) findViewById(R.id.forgot_et_phone_no);
            btn_submit = (Button) findViewById(R.id.forgot_btn_send);

            btn_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        progressDialog.show();
                        String phone = et_phone.getText().toString();
                        if (phone.equals("")) {
                            showDialogAlert(getString(R.string.alert_login_warning), getString(R.string.alert_forgot_input_phone_no));
                        } else {
                            RequestForgot requestForgot = new RequestForgot(phone);
                            Call<ResponseService> call = customerPlaceHolderApi.getForGotPassowrd(requestForgot);

                            call.enqueue(new Callback<ResponseService>() {
                                @Override
                                public void onResponse(Call<ResponseService> call, Response<ResponseService> response) {
                                    progressDialog.dismiss();

                                    if (!response.isSuccessful()) {
                                        return;
                                    }

                                    responseService = response.body();
                                    responseData(responseService);
                                }

                                @Override
                                public void onFailure(Call<ResponseService> call, Throwable t) {
//                                Toast.makeText(LoginActivity.this, "ERROR " + t.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e){
            e.printStackTrace();
            showDialogAlert(getString(R.string.alert_warning), getString(R.string.alert_warning_client_error));
        }

    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void responseData(ResponseService responseData) {
        if (responseData.getResCode().equals(getString(R.string.code_success))) {
//            progressDialog.show();
            showDialogAlert(getString(R.string.alert_warning), responseData.getMessage());
        } else {
//            progressDialog.show();
            showDialogAlert(getString(R.string.alert_warning), responseData.getMessage());
        }
    }

    public void gotoTutorailActivity() {
        try {
            Intent intent = new Intent(ForgotPasswordActivity.this, TutorialActivity.class);
            // Clear Stack Activity
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            setAnimationsBackPage();
        } catch (Exception e) {
            e.printStackTrace();
            showDialogAlert(getString(R.string.alert_warning), getString(R.string.alert_warning_client_error));
        }
    }

    public void setAnimationsBackPage() {
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void showDialogAlert(String title, String subTitle){
        txt_header.setText(title);
        txt_sub_header.setText(subTitle);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public void setAnimationsNextPage() {
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

}
