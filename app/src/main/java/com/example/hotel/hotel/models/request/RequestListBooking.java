package com.example.hotel.hotel.models.request;

public class RequestListBooking {

    private int CusID = 0;
    private String Status = "";

    public RequestListBooking() {
    }

    public RequestListBooking(int cusID, String status) {
        CusID = cusID;
        Status = status;
    }

    public int getCusID() {
        return CusID;
    }

    public void setCusID(int cusID) {
        CusID = cusID;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}
